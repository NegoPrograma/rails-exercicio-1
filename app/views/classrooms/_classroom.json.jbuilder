json.extract! classroom, :id, :Year, :Shift, :Course, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)

json.extract! discipline, :id, :Type, :Series, :Schedule, :Availability, :created_at, :updated_at
json.url discipline_url(discipline, format: :json)

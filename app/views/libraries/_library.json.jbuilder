json.extract! library, :id, :Book_Name, :Book_Code, :Availability, :Quantity, :Student_id, :created_at, :updated_at
json.url library_url(library, format: :json)

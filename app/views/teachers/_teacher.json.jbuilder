json.extract! teacher, :id, :Name, :Workload, :Cpf, :Discipline_id, :Classroom_id, :created_at, :updated_at
json.url teacher_url(teacher, format: :json)

Rails.application.routes.draw do
  resources :disciplines
  resources :classrooms
  resources :students
  resources :libraries
  resources :teachers
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

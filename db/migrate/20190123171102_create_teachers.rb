class CreateTeachers < ActiveRecord::Migration[5.2]
  def change
    create_table :teachers do |t|
      t.string :Name
      t.integer :Workload
      t.string :Cpf
      t.references :Discipline, foreign_key: true
      t.references :Classroom, foreign_key: true

      t.timestamps
    end
  end
end

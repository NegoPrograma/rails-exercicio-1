class CreateLibraries < ActiveRecord::Migration[5.2]
  def change
    create_table :libraries do |t|
      t.string :Book_Name
      t.integer :Book_Code
      t.boolean :Availability
      t.integer :Quantity
      t.references :Student, foreign_key: true

      t.timestamps
    end
  end
end

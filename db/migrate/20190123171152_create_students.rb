class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :Name
      t.string :Cpf
      t.string :Registration
      t.references :Classroom, foreign_key: true

      t.timestamps
    end
  end
end

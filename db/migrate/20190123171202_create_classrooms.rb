class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.integer :Year
      t.string :Shift
      t.string :Course

      t.timestamps
    end
  end
end

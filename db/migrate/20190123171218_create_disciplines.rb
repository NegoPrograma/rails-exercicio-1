class CreateDisciplines < ActiveRecord::Migration[5.2]
  def change
    create_table :disciplines do |t|
      t.string :Type
      t.integer :Series
      t.string :Schedule
      t.boolean :Availability

      t.timestamps
    end
  end
end

class AddNameToDisciplines < ActiveRecord::Migration[5.2]
  def change
    add_column :disciplines, :name, :string
  end
end

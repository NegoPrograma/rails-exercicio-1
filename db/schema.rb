# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_23_172757) do

  create_table "classrooms", force: :cascade do |t|
    t.integer "Year"
    t.string "Shift"
    t.string "Course"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disciplines", force: :cascade do |t|
    t.string "Type"
    t.integer "Series"
    t.string "Schedule"
    t.boolean "Availability"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
  end

  create_table "libraries", force: :cascade do |t|
    t.string "Book_Name"
    t.integer "Book_Code"
    t.boolean "Availability"
    t.integer "Quantity"
    t.integer "Student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["Student_id"], name: "index_libraries_on_Student_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "Name"
    t.string "Cpf"
    t.string "Registration"
    t.integer "Classroom_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["Classroom_id"], name: "index_students_on_Classroom_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.string "Name"
    t.integer "Workload"
    t.string "Cpf"
    t.integer "Discipline_id"
    t.integer "Classroom_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["Classroom_id"], name: "index_teachers_on_Classroom_id"
    t.index ["Discipline_id"], name: "index_teachers_on_Discipline_id"
  end

end
